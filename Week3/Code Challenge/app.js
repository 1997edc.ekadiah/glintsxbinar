// Import expressjs to make API
const express = require('express') //import expressjs
const app = express() //make instance object 

// If user access localhost:3000/Eka this will be run
app.get('/Eka',(req, res) => {
    console.log("You're accessing hello!")
    res.render('eka.ejs')
})

app.listen(3000);