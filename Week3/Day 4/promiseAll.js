// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Async function */
async function mergedContent() {
  try {
    /* This recommended */
    const result = await Promise.all([
      read('content/1.txt'),
      read('content/2.txt'),
      read('content/3.txt'),
      read('content/4.txt'),
      read('content/5.txt'),
      read('content/6.txt'),
      read('content/7.txt'),
      read('content/8.txt'),
      read('content/9.txt'),
      read('content/10.txt'),
    ])
    /* End this recommended */
    await writeFile('content/gabung.txt', result.join(''))
  } catch (error) {
    throw error
  }

  // The best practice is:
  // return promise, not return value of promise
  // not also return use await
  return read('content/gabung.txt')
}
/* Async function end */

// console.log(mergedContent()); // Promise (Pending)

// Start promise
mergedContent() // process read/write
  .then(result => {
    console.log('Success to read and write file, content: ', result); // If success read/write file
  }).catch(error => {
    console.log('Failed to read/write file, content: ', error); // If error read/write file
  }).finally(() => {
    console.log('Mantap!'); // run after success or error
  })