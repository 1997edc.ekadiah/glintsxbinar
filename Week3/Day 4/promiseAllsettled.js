// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const
  read = readFile('utf-8'),
  files = ['content/1.txt', 'content/2.txt', 'content/3.txt', 'content/4.txt', 'content/5.txt', 'content/6.txt', 'content/7.txt', 'content/8.txt', 'content/9.txt', 'content/10.txt']

// start promise allSettled
Promise.allSettled(files.map(file => read(`${file}`)))
  .then(results => {
    // const existsContent = results.filter(result => result.status === 'fulfilled')
    // isi dari existsContent: [{ status: 'fulfilled', value: 'isi content 1' }, ...]
    // dan seterusnya kecuali content 5
    console.log(results)
  })