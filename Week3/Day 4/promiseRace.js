
// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
  fs.readFile(file, options, (error, content) => {
    if (error) {
      return reject(error)
    }

    return fulfill(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Promise race */
Promise.race([read('content/1.txt'), read('content/2.txt'), read('content/3.txt'), read('content/4.txt'), read('content/5.txt'), read('content/6.txt'), read('content/7.txt'), read('content/8.txt'), read('content/9.txt'), read('content/10.txt'), ])
  .then((value) => {
    console.log(value);
  })
  .catch(error => {
    console.log(error);
  })
/* Promise race end */